#!/usr/bin/python3
import pygsheets
from multiprocessing import Pool

gc = pygsheets.authorize(service_file='./German words-be22c27337ba.json')
sh = gc.open_by_key('10D-NsnwxiJUuELnpKxGdsjrwiiBKmrWtT5UlPFaGRTE')
TITLE = 'Words'

def getWordsheet():
    return sh.worksheet('title', TITLE)

def initWordSheet():
    '''
    Creates a new worksheet for saving words
    if sheet does not exist
    '''
    try:
        wordsheet = getWordsheet()
    except pygsheets.WorksheetNotFound:
        wordsheet = sh.add_worksheet(TITLE)
    return wordsheet

def readWords():
    wordsheet = getWordsheet()
    return wordsheet.get_all_values()

def findWordsG2F(word, wordmatrix):
    '''
    Returns the sublist and the 0-indexed row number
    '''
    return (words for words in wordmatrix if words[0] == word),\
        [words[0] for words in wordmatrix].index(word)

def fillMissingWords(local):
    remote = readCells()

def forkingPICKLER(args):
    wks = getWordsheet()
    word, x, y = args
    label = f'{chr(65+x)}{y+1}'
    print(label, word)
    wks.update_cell(label, word)

def cleanFillWords(wordmatrix):
    size = len(wordmatrix), len(wordmatrix[0])
    pool = Pool(size[0] * size[1])
    print(pool.map(forkingPICKLER, [(wordmatrix[j][i], i, j) for j in range(size[0]) for i in range(size[1])]))
